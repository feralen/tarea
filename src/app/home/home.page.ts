import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public items: string[];

  constructor() {

    this.items = [
    'item 1',
    'item 2',
    'item 3',
    'item 4',
    'item 5'
    ];
  }

  reorderItems(ev){
    console.log(ev);
    let itemToMove = this.items.splice(ev.detail.from, 1)[0];
    this.items.splice(ev.detail.to, 0, itemToMove);
  }

  save(){
    console.log(this.items);
  }

}
