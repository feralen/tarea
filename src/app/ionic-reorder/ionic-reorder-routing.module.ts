import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IonicReorderPage } from './ionic-reorder.page';

const routes: Routes = [
  {
    path: '',
    component: IonicReorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IonicReorderPageRoutingModule {}
