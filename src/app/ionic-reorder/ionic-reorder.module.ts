import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IonicReorderPageRoutingModule } from './ionic-reorder-routing.module';

import { IonicReorderPage } from './ionic-reorder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicReorderPageRoutingModule
  ],
  declarations: [IonicReorderPage]
})
export class IonicReorderPageModule {}
