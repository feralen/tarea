import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IonicReorderPage } from './ionic-reorder.page';

describe('IonicReorderPage', () => {
  let component: IonicReorderPage;
  let fixture: ComponentFixture<IonicReorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IonicReorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IonicReorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
